@extends('layouts.master')
@section('content')
    <article class="content forms-page">
        <div class="title-block">
            <h3 class="title"> Bank Lookup Form </h3>
        </div>
        <section class="section">
            <div class="row sameheight-container">
                <div class="col-md-12">
                    <div class="card card-block sameheight-item" style="height: 1600px">
                        <form role="form" method="post" action="{{route('addBank')}}">

                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="bankIdInput">Bank ID
                                    </label>
                                    <input type="text" class="form-control" value="{{$bankId}}" id="bankIdInput" name="bankId" readonly>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="bankNameInput">Bank Name</label>
                                    <input type="text" class="form-control" id="bankNameInput" placeholder="Bank name" name="bankName" required>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="branchNameInput">Branch Name</label>
                                    <input type="text" class="form-control" id="branchNameInput" placeholder="Branch Name" name="branchName" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="branchCodeInput">Branch Code</label>
                                    <input type="text" class="form-control" id="branchCodeInput" placeholder="Branch Code" name="branchCode" required>
                                </div>
                                <fieldset class="form-group col-lg-8">
                                    <label class="control-label" for="remarksInput">Remarks</label>
                                    <textarea type="text" class="form-control" id="remarksInput" name="remarks" placeholder="Remarks" required></textarea>
                                </fieldset>
                            </div>
                            <fieldset class="form-group col-lg-12">
                                <button class="btn btn-success text-primary" name="addBank" type="submit">Add</button>
                            </fieldset>
                            <input type="hidden" value="{{Session::token()}}" name="_token">
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> All Banks </h3>
                            </div>
                            <section class="example">
                                <div class="table-flip-scroll">
                                    <table class="table table-striped table-bordered table-hover flip-content">
                                        <thead class="flip-header">
                                        <tr>
                                            <th>Bank ID</th>
                                            <th>Bank name</th>
                                            <th>Branch Name</th>
                                            <th>Branch Code</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($banks as $bank)
                                                <tr class=" ">
                                                    <td>{{$bank->bank_id}}</td>
                                                    <td>{{$bank->bank_name}}</td>
                                                    <td>{{$bank->branch_name}}</td>
                                                    <td>{{$bank->branch_code}}</td>
                                                    <td class="center">
                                                        <a>
                                                            <i class="text-primary fa fa-edit"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@endsection
