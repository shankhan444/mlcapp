@extends('layouts.master')
@section('content')
<article class="content dashboard-page">
    <section class="section">
        <div class="row sameheight-container">
            {{--<div class="col col-xs-12 col-sm-12 col-md-6 col-xl-6 history-col">--}}
                {{--<div class="card sameheight-item" data-exclude="xs">--}}
                    {{--<div class="card-header card-header-sm bordered">--}}
                        {{--<div class="header-block">--}}
                            {{--<h3 class="title">Regions And Provinces</h3>--}}
                        {{--</div>--}}
                        {{--<ul class="nav nav-tabs pull-right" role="tablist">--}}
                            {{--<li class="nav-item"> <a class="nav-link " href="#visits" role="tab" data-toggle="tab">Visits</a> </li>--}}
                            {{--<li class="nav-item"> <a class="nav-link active" href="#downloads" role="tab" data-toggle="tab">Downloads</a> </li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                    {{--<div class="card-block">--}}
                        {{--<div class="tab-content">--}}
                            {{--<div role="tabpanel" class="tab-pane active fade in" id="visits">--}}
                                {{--<p class="title-description"> Number of Regions In A Province </p>--}}
                                {{--<div id="dashboard-visits-chart"></div>--}}
                            {{--</div>--}}
                            {{--<div role="tabpanel" class="tab-pane fade" id="downloads">--}}
                                {{--<p class="title-description"> Number of Regions In A Province </p>--}}
                                {{--<div id="dashboard-downloads-chart"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col col-xs-12 col-sm-12 col-md-6 col-xl-6 history-col">--}}
                {{--<div class="card sameheight-item" data-exclude="xs">--}}
                    {{--<div class="col-xl-12">--}}
                        {{--<div class="card sameheight-item sales-breakdown" data-exclude="xs,sm,lg">--}}
                            {{--<div class="card-header">--}}
                                {{--<div class="header-block">--}}
                                    {{--<h3 class="title"> Sales breakdown </h3>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="card-block">--}}
                                {{--<div class="dashboard-sales-breakdown-chart" id="dashboard-sales-breakdown-chart"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}



            <canvas id="myChart" width="400" height="400"></canvas>
        </div>
    </section>
</article>
<footer class="footer">
    <script>

    </script>
</footer>
@endsection
