@extends('layouts.master')
@section('content')
    <article class="content forms-page">
        <div class="title-block">
            <h3 class="title"> Branch Lookup Form </h3>
        </div>
        <section class="section">
            <div class="row sameheight-container">
                <div class="col-md-12">
                    <div class="card card-block sameheight-item" style="height: 1600px">
                        <form role="form" method="post" action="">
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="branchInput">Branch Name</label>
                                    <input type="text" class="form-control" id="branchInput" placeholder="Branch name" name="branchName" required>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="branchAbbInput">Short Name</label>
                                    <input type="text" class="form-control" id="branchAbbInput" placeholder="Short Name" name="branchAbbreviation" required>
                                </div>
                            </div>
                            {{csrf_field()}}
                            <fieldset class="form-group col-lg-12">
                                <button class="btn btn-success text-primary" name="addBranch" type="submit" >Add Branch</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> All Branches </h3>
                            </div>
                            <section class="example">
                                <div class="table-flip-scroll">
                                    <table class="table table-striped table-bordered table-hover flip-content">
                                        <thead class="flip-header">
                                        <tr>
                                            <th>Branch Name</th>
                                            <th>Short Name</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{--@foreach($branches as $branch)--}}
                                            {{--<tr class=" ">--}}
                                                {{--<td>{{$branch->desig_name}}</td>--}}
                                                {{--<td>{{$branch->desig_abbr}}</td>--}}
                                                {{--@if($branch->active=="Y")--}}
                                                    {{--<td><input type="checkbox" disabled checked="checked"></td>--}}
                                                {{--@else--}}
                                                    {{--<td><input type="checkbox" disabled></td>--}}
                                                {{--@endif--}}
                                                {{--<td class="center">--}}

                                                    {{--<a href="#" data-toggle="modal" data-target="#editModal{{$designation->desig_id}}">--}}
                                                        {{--<i class="text-primary fa fa-edit"></i>--}}
                                                    {{--</a>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}
                                            {{--<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="editModal{{$designation->desig_id}}">--}}
                                                {{--<div class="modal-dialog">--}}
                                                    {{--<div class="modal-content">--}}
                                                        {{--<div class="modal-header">--}}
                                                            {{--<h5 class="modal-title">Edit Form Of Designation : {{$designation->desig_name}} <em class="text-primary"> </em> </h5>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="modal-body">--}}
                                                            {{--<section class="section">--}}
                                                                {{--<div class="row sameheight-container">--}}
                                                                    {{--<div class="col-md-12">--}}
                                                                        {{--<div class="card card-block sameheight-item" style="height: 1600px">--}}
                                                                            {{--@include('includes.errors')--}}
                                                                            {{--<form role="form" method="post" action="{{route('updateDesignation')}}">--}}
                                                                                {{--<input type="hidden" name="designationId" value="{{$designation->desig_id}}">--}}
                                                                                {{--<div class="row">--}}
                                                                                    {{--<div class="form-group col-lg-4">--}}
                                                                                        {{--<label class="control-label" for="designationInput">Designation Name</label>--}}
                                                                                        {{--<input type="text" class="form-control" id="designationInput" value="{{$designation->desig_name}}" name="designationName" required>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="form-group col-lg-4">--}}
                                                                                        {{--<label class="control-label" for="designationAbbInput">Abbreviation</label>--}}
                                                                                        {{--<input type="text" class="form-control" id="designationAbbInput" value="{{$designation->desig_abbr}}" name="designationAbbr" required>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="form-group col-lg-4">--}}
                                                                                        {{--<label class="control-label" for="designationStatusSelect">Is Active ?</label>--}}
                                                                                        {{--<select type="text" class="form-control" id="designationStatusSelect" name="designationActive" required>--}}
                                                                                            {{--@if($designation->active=="Y")--}}
                                                                                                {{--<option selected  value="{{$designation->active}}">Yes</option>--}}
                                                                                                {{--<option value="N">No</option>--}}
                                                                                            {{--@else--}}
                                                                                                {{--<option selected  value="{{$designation->active}}">No</option>--}}
                                                                                                {{--<option value="Y">Yes</option>--}}
                                                                                            {{--@endif--}}


                                                                                        {{--</select>--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                                {{--{{csrf_field()}}--}}
                                                                                {{--<fieldset class="form-group col-lg-12 ">--}}
                                                                                    {{--<button class="btn btn-success text-primary" name="addDesignation" type="submit" >Update Designation</button>--}}
                                                                                {{--</fieldset>--}}
                                                                            {{--</form>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</section>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--@endforeach--}}
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@endsection
