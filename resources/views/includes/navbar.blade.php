<aside class="sidebar">
    <div class="sidebar-container">
        <div class="sidebar-header">
            <div class="brand">
                <div class="logo">
                    <span class="l l1"></span>
                    <span class="l l2"></span>
                    <span class="l l3"></span>
                    <span class="l l4"></span>
                    <span class="l l5"></span>
                </div>
                 </div>
        </div>
        <nav class="menu">
            <ul class="nav metismenu" id="sidebar-menu">
                <li class="active">
                    <a href="{{route('dashboard')}}">
                        <i class="fa fa-home"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="">
                        <i class="fa fa-user"></i> HR
                        <i class="fa arrow"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="">
                                <i class="fa fa-angle-down"></i>
                                &nbsp;&nbsp;Forms
                            </a>
                            <ul>
                                <li>
                                    <a href="{{route('employeeAdd')}}">
                                        <i class="fa fa-user-plus"></i>
                                        &nbsp;Employee Add
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-angle-down"></i>
                                &nbsp;Reports
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="">
                        <i class="fa fa-user-secret"></i> Administration
                        <i class="fa arrow"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="">
                                <i class="fa fa-gears"></i>
                                &nbsp;&nbsp;Setups
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul>
                                <li>
                                    <a href="">
                                        <i class="fa fa-search"></i>
                                        &nbsp;Lookups
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{route('AccommodationLookup')}}">
                                                <i class="fa fa-home"></i>
                                                &nbsp;&nbsp;Accommodation
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('AccommodationTypeLookup')}}">
                                                <i class="fa fa-home"></i>
                                                &nbsp;&nbsp;Accommodation Type
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('bankLookup')}}">
                                                <i class="fa fa-bank"></i>
                                                &nbsp;&nbsp;Bank
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('BranchesLookup')}}">
                                                <i class="fa fa-braille"></i>
                                                &nbsp;&nbsp;Branches
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('BloodGroupLookup')}}">
                                                <i class="fa fa-tint"></i>
                                                &nbsp;&nbsp;Blood Group
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('ChartsOfAccountLookup')}}">
                                                <i class="fa fa-area-chart"></i>
                                                &nbsp;&nbsp;Charts Of Account
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('ClassificationOfCantonmentsLookup')}}">
                                                <i class="fa fa-tags"></i>
                                                &nbsp;&nbsp;Classification Of Cantonments
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('DesignationsLookup')}}">
                                                <i class="fa fa-deaf"></i>
                                                &nbsp;&nbsp;Designations
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('DegreesLookup')}}">
                                                <i class="fa fa-book"></i>
                                                &nbsp;&nbsp;Degrees
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('DistrictLookup')}}">
                                                <i class="fa fa-cc-discover"></i>
                                                &nbsp;&nbsp;District
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('DisabilitiesLookup')}}">
                                                <i class="fa fa-wheelchair"></i>
                                                &nbsp;&nbsp;Disabilities
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('EmployeeCategoryLookup')}}">
                                                <i class="fa fa-users"></i>
                                                &nbsp;&nbsp;Employee Category
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('GendersLookup')}}">
                                                <i class="fa fa-genderless"></i>
                                                &nbsp;&nbsp;Genders
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('InstitutesLookup')}}">
                                                <i class="fa fa-indent"></i>
                                                &nbsp;&nbsp;Institutes
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('LocationLookup')}}">
                                                <i class="fa fa-map-marker"></i>
                                                &nbsp;&nbsp;Location
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('MajorPenaltiesLookup')}}">
                                                <i class="fa fa-bullseye"></i>
                                                &nbsp;&nbsp;Major Penalties
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('MinorPenaltiesLookup')}}">
                                                <i class="fa fa-dot-circle-o"></i>
                                                &nbsp;&nbsp;Minor Penalties
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('MinorDisabilitiesLookup')}}">
                                                <i class="fa fa-wheelchair"></i>
                                                &nbsp;&nbsp;Minor Disabilities
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('OfficesLookup')}}">
                                                <i class="fa fa-briefcase"></i>
                                                &nbsp;&nbsp;Offices
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('PayDuringLeaveLookup')}}">
                                                <i class="fa fa-money"></i>
                                                &nbsp;&nbsp;Pay During Leave
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('ProvincesLookup')}}">
                                                <i class="fa fa-area-chart"></i>
                                                &nbsp;&nbsp;Provinces
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('PensionTypeLookup')}}">
                                                <i class="fa fa-sort-amount-asc"></i>
                                                &nbsp;&nbsp;Pension Type
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{route('RegionsLookup')}}">
                                                <i class="fa fa-sort-amount-asc"></i>
                                                &nbsp;&nbsp;Regions
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('ReligionsLookup')}}">
                                                <i class="fa fa-rebel"></i>
                                                &nbsp;&nbsp;Religions
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('ScaleLookup')}}">
                                                <i class="fa fa-balance-scale"></i>
                                                &nbsp;&nbsp;Scale
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('ServiceNatureLookup')}}">
                                                <i class="fa fa-server"></i>
                                                &nbsp;&nbsp;Service Nature
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{route('WOGLookup')}}">
                                                <i class="fa fa-won"></i>
                                                &nbsp;&nbsp;WOG
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <footer class="sidebar-footer">

    </footer>
</aside>
<div class="sidebar-overlay" id="sidebar-overlay"></div>