<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MLCBanks extends Controller
{
    protected $tableName,$connection;

    public function __construct(){
       $this->tableName = 'MLC.MLC_BANK';
       $this->connection = DB::connection('oracle');
    }

    public function index()  {
        $banks = $this->getAllBanks();
        $bankId = $this->getNextBankID();
        return view('lookups.banks',compact(['banks', 'bankId']));
    }

    public function getAllBanks() {
         $banks = $this->connection->select('select * from ' . $this->tableName);
         return $banks;
    }

    public function getNextBankID() {
        $bankId = $this->connection->select('select COALESCE(max(BANK_ID)+1, 1) as NEW from ' . $this->tableName);
        return $bankId[0]->new;
    }

    public function store() {
        $this->validate(request() , [
            'bankName' => 'required',
            'branchName' => 'required',
            'branchCode' => 'required'
        ]);

        $bankData = [
            "BANK_ID"   => $this->getNextBankID(),
            "BANK_NAME"   => request('bankName'),
            "BRANCH_NAME" => request('branchName'),
            "BRANCH_CODE" => request('branchCode'),
            "REMARKS"     => request('remarks')
        ];

        $this->connection->table($this->tableName)->insert($bankData);

        return redirect()->route('bankLookup');
    }
}
