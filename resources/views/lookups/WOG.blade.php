@extends('layouts.master')
@section('content')
    <article class="content forms-page">
        <div class="title-block">
            <h3 class="title"> WOG Lookup Form </h3>
        </div>
        <section class="section">
            <div class="row sameheight-container">
                <div class="col-md-12">
                    <div class="card card-block sameheight-item" style="height: 1600px">
                        <form role="form" method="post" action="">

                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="employeeIdInput">WOG ID
                                    </label>
                                    <input type="text" class="form-control" id="employeeIdInput" name="employeeId" disabled>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="employeeCNICInput">WOG Name</label>
                                    <input type="text" class="form-control" id="employeeCNICInput" placeholder="WOG name" name="employeeCNIC">
                                </div>
                            </div>
                            <fieldset class="form-group col-lg-12">
                                <button class="btn btn-success text-primary" >Add</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> All WOG </h3>
                            </div>
                            <section class="example">
                                <div class="table-flip-scroll">
                                    <table class="table table-striped table-bordered table-hover flip-content">
                                        <thead class="flip-header">
                                        <tr>
                                            <th>WOG ID</th>
                                            <th>WOG Name</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>WOG Name </td>
                                            <td class="center">
                                                <a>
                                                    <i class="text-primary fa fa-edit"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@endsection
