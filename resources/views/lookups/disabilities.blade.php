@extends('layouts.master')
@section('content')
    <article class="content forms-page">
        <div class="title-block">
            <h3 class="title"> Disability Lookup Form </h3>
        </div>
        <section class="section">
            <div class="row sameheight-container">
                <div class="col-md-12">
                    <div class="card card-block sameheight-item" style="height: 1600px">
                        @include('includes.errors')
                        <form role="form" method="post" action="">
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="disabilityNameInput">Disability Name</label>
                                    <input type="text" class="form-control" id="disabilityNameInput" placeholder="Disability name" name="disabilityName" required>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="disabilityStatusSelect">Is Active ?</label>
                                    <select type="text" class="form-control" id="disabilityStatusSelect" name="disabilitieActive" required>
                                        <option selected disabled value="">--Select Status--</option>
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                                </div>
                            </div>
                            {{csrf_field()}}
                            <fieldset class="form-group col-lg-12 ">
                                <button class="btn btn-success text-primary" name="addDisability" type="submit" >Add Disability</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> All Disabilities </h3>
                            </div>
                            <section class="example">
                                <div class="table-flip-scroll">
                                    <table class="table table-striped table-bordered table-hover flip-content">
                                        <thead class="flip-header">
                                        <tr>
                                            <th>Disability Name</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        {{--<tbody>--}}
                                        {{--                                        {{dd($disabilities)}}--}}
                                        {{--@foreach($disabilities as $disabilitie)--}}
                                            {{--<tr class=" ">--}}
                                                {{--<td>{{$disabilitie->desig_name}}</td>--}}
                                                {{--<td>{{$disabilitie->desig_abbr}}</td>--}}
                                                {{--@if($disabilitie->active=="Y")--}}
                                                    {{--<td><input type="checkbox" disabled checked="checked"></td>--}}
                                                {{--@else--}}
                                                    {{--<td><input type="checkbox" disabled></td>--}}
                                                {{--@endif--}}
                                                {{--<td class="center">--}}

                                                    {{--<a href="#" data-toggle="modal" data-target="#editModal{{$disabilitie->desig_id}}">--}}
                                                        {{--<i class="text-primary fa fa-edit"></i>--}}
                                                    {{--</a>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}
                                            {{--<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="editModal{{$disabilitie->desig_id}}">--}}
                                                {{--<div class="modal-dialog">--}}
                                                    {{--<div class="modal-content">--}}
                                                        {{--<div class="modal-header">--}}
                                                            {{--<h5 class="modal-title">Edit Form Of Disability : {{$disabilitie->desig_name}} <em class="text-primary"> </em> </h5>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="modal-body">--}}
                                                            {{--<section class="section">--}}
                                                                {{--<div class="row sameheight-container">--}}
                                                                    {{--<div class="col-md-12">--}}
                                                                        {{--<div class="card card-block sameheight-item" style="height: 1600px">--}}
                                                                            {{--@include('includes.errors')--}}
                                                                            {{--<form role="form" method="post" action="{{route('updateDisability')}}">--}}
                                                                                {{--<input type="hidden" name="disabilitieId" value="{{$disabilitie->desig_id}}">--}}
                                                                                {{--<div class="row">--}}
                                                                                    {{--<div class="form-group col-lg-4">--}}
                                                                                        {{--<label class="control-label" for="disabilitieInput">Disability Name</label>--}}
                                                                                        {{--<input type="text" class="form-control" id="disabilitieInput" value="{{$disabilitie->desig_name}}" name="disabilitieName" required>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="form-group col-lg-4">--}}
                                                                                        {{--<label class="control-label" for="disabilitieAbbInput">Abbreviation</label>--}}
                                                                                        {{--<input type="text" class="form-control" id="disabilitieAbbInput" value="{{$disabilitie->desig_abbr}}" name="disabilitieAbbr" required>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="form-group col-lg-4">--}}
                                                                                        {{--<label class="control-label" for="disabilitieStatusSelect">Is Active ?</label>--}}
                                                                                        {{--<select type="text" class="form-control" id="disabilitieStatusSelect" name="disabilitieActive" required>--}}
                                                                                            {{--@if($disabilitie->active=="Y")--}}
                                                                                                {{--<option selected  value="{{$disabilitie->active}}">Yes</option>--}}
                                                                                                {{--<option value="N">No</option>--}}
                                                                                            {{--@else--}}
                                                                                                {{--<option selected  value="{{$disabilitie->active}}">No</option>--}}
                                                                                                {{--<option value="Y">Yes</option>--}}
                                                                                            {{--@endif--}}


                                                                                        {{--</select>--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                                {{--{{csrf_field()}}--}}
                                                                                {{--<fieldset class="form-group col-lg-12 ">--}}
                                                                                    {{--<button class="btn btn-success text-primary" name="addDisability" type="submit" >Update Disability</button>--}}
                                                                                {{--</fieldset>--}}
                                                                            {{--</form>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</section>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--@endforeach--}}
                                        {{--</tbody>--}}
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@endsection
