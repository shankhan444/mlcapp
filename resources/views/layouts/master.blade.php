<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <title>MLC App</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{URL::to('assets/css/vendor.css')}}">
    <link rel="stylesheet" href="{{URL::to('assets/css/app.css')}}">
</head>
<body>
<div class="main-wrapper">
    <div class="app" id="app">
        @include('includes.header')
        @include('includes.navbar')
        @yield('content')
        @include('includes.footer')
    </div>
</div>
</body>
</html>
