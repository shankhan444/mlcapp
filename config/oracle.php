<?php

return [
    'oracle' => [
        'driver'        => 'oracle',
        'tns'           => env('ORACLE_DB_TNS', '192.168.100.22/prod'),
        'host'          => env('ORACLE_DB_HOST', '192.168.100.22'),
        'port'          => env('ORACLE_DB_PORT', '1521'),
        'database'      => env('ORACLE_DB_DATABASE', 'MLC'),
        'username'      => env('ORACLE_DB_USERNAME', 'ccb'),
        'password'      => env('ORACLE_DB_PASSWORD', 'ccantdb'),
        'charset'       => env('ORACLE_DB_CHARSET', 'AL32UTF8'),
        'prefix'        => env('ORACLE_DB_PREFIX', ''),
        'prefix_schema' => env('ORACLE_DB_SCHEMA_PREFIX', ''),
    ],
];