<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MLCDesignations extends Controller
{

    protected $tableName,$connection;

    public function __construct(){
       $this->tableName = 'MLC.MLC_DESIGNATIONS';
       $this->connection = DB::connection('oracle');
    }

    public function index()  {
        $designations = $this->getAll();
        return view('lookups.designations',compact(['designations']));
    }

    public function getAll() {
         $all = $this->connection->table($this->tableName)->get();
         return $all;
    }

    public function getNextID() {
        $id = $this->connection->select('select COALESCE(max(DESIG_ID)+1, 1) as NEW from ' . $this->tableName);
        return $id[0]->new;
    }

    public function store() {
        $this->validate(request() , [
            'designationName' => 'required',
            'designationAbbr' => 'required',
            'designationActive' => 'required'
        ]);


        $designation = [
            "DESIG_ID"   => $this->getNextID(),
            "DESIG_NAME"   => request('designationName'),
            "DESIG_ABBR" => request('designationAbbr'),
            "ACTIVE" => request('designationActive')
        ];

        $this->connection->table($this->tableName)->insert($designation);

        return redirect()->route('DesignationsLookup');
    }

    public function update() {
         $this->validate(request() , [
            'designationId' => 'required',
            'designationName' => 'required',
            'designationAbbr' => 'required',
            'designationActive' => 'required'
        ]);

        $this->connection->table($this->tableName)->where('DESIG_ID', request('designationId'))->update([
            "DESIG_NAME"   => request('designationName'),
            "DESIG_ABBR" => request('designationAbbr'),
            "ACTIVE" => request('designationActive')
        ]);

        return redirect()->route('DesignationsLookup');
    }
}
