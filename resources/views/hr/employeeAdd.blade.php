@extends('layouts.master')
@section('content')
<article class="content forms-page">
    <div class="title-block">
        <h3 class="title"> Employee Form </h3>
    </div>
    <section class="section">
        <div class="row sameheight-container">
            <div class="col-md-12">
                <div class="card card-block sameheight-item" style="height: 1600px">
                    <form role="form" method="post" action="">
                        <div class="row">
                            <fieldset class="form-group col-lg-6">
                                <label class="control-label" for="employeeIdInput">Employee ID
                                </label>
                                <input type="text" class="form-control" id="employeeIdInput" name="employeeId" disabled>

                                <label class="control-label" for="employeeNameInput">Employee Name</label>
                                <input type="text" class="form-control" id="employeeNameInput" placeholder="Employee Name" name="employeeName">

                                <label class="control-label" for="fatherNameInput">Father Name</label>
                                <input type="text" class="form-control" id="fatherNameInput" placeholder="Father Name" name="fatherName">

                                <label class="control-label" for="employeeContactNumberInput">Contact Number</label>
                                <input type="number" class="form-control" id="employeeContactNumberInput" placeholder="Contact Number" name="contactNumber">
                            </fieldset>
                            <fieldset class="form-group col-lg-6">
                                <div class="container">
                                    <div class="imageupload panel panel-default">
                                        <div class="panel-heading clearfix">
                                            <h3 class="panel-title pull-left">Upload Image</h3>
                                            <image src="" class="thumbnail" />
                                            <input type="file" name="employeeImage">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label class="control-label" for="employeeEmailInput">Email</label>
                                <input type="email" class="form-control" id="employeeEmailInput" placeholder="Employee Email" name="employeeEmail">
                            </div>
                            <div class="form-group col-lg-4">
                                <label class="control-label" for="employeeCNICInput">CNIC</label>
                                <input type="text" class="form-control" id="employeeCNICInput" placeholder="Employee CNIC" name="employeeCNIC">
                            </div>
                            <div class="form-group col-lg-4">
                                <label class="control-label" for="employeeDOBInput">Date Of Birth</label>
                                <input type="date" class="form-control" id="employeeDOBInput" placeholder="Employee Date Of Birth" name="employeeDOB">
                            </div>
                        </div>
                        <div class="row">

                            <div class="form-group col-lg-4">
                                <label class="control-label" for="formGroupExampleInput">Gender</label>
                                <select class="form-control">
                                    <option selected disabled>--Select Gender--</option>
                                    <option value=""> Will Come From LOV</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label class="control-label" for="employeeProvinceInput">Province</label>
                                <select class="form-control" id="employeeProvinceInput" name="employeeProvinceId">
                                    <option selected disabled>--Select Province--</option>
                                    <option value="">Province Will Come From LOV</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label class="control-label" for="employeeDistrictInput">District</label>
                                <select class="form-control" id="employeeDistrictInput" name="employeeDistrict">
                                    <option selected Disabled>--Select Province First--</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label class="control-label" for="employeeReligionInput">Religion</label>
                                <select class="form-control" id="employeeReligionInput" name="employeeReligion">
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label class="control-label" for="employeeProvinceInput">Province</label>
                                <select class="form-control" id="employeeProvinceInput" name="employeeProvinceId">
                                    <option selected disabled>--Select Province--</option>
                                    <option value="">Province Will Come From LOV</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label class="control-label" for="employeeDistrictInput">District</label>
                                <select class="form-control" id="employeeDistrictInput" name="employeeDistrict">
                                    <option selected Disabled>--Select Province First--</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label class="control-label" for="employeeReligionInput">Religion</label>
                                <select class="form-control" id="employeeReligionInput" name="employeeReligion">
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>

                        </div>
                        <div class="row">
                            <fieldset class="form-group col-lg-12">
                                <label class="control-label" for="employeeAddressInput">Address</label>
                                <textarea type="text" class="form-control" id="employeeAddressInput" name="employeeAddress" placeholder="Employee Address"></textarea>
                            </fieldset>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label class="control-label" for="employeeBPSInput">BPS</label>
                                <select class="form-control" id="employeeBPSInput" name="employeeBPS">
                                    <option selected disabled>--Select BPS--</option>
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="control-label" for="employeeDesignationInput">Designation</label>
                                <select class="form-control" id="employeeDesignationInput" name="employeeDesignation">
                                    <option selected disabled>--Select Designation--</option>
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="control-label" for="employeeCategoryInput">Category</label>
                                <select class="form-control" id="employeeCategoryInput" name="employeeCategory">
                                    <option selected disabled>--Select Category--</option>
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label class="control-label" for="employeeBPSInput">BPS</label>
                                <select class="form-control" id="employeeBPSInput" name="employeeBPS">
                                    <option selected disabled>--Select BPS--</option>
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="control-label" for="employeeDesignationInput">Designation</label>
                                <select class="form-control" id="employeeDesignationInput" name="employeeDesignation">
                                    <option selected disabled>--Select Designation--</option>
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="control-label" for="employeeCategoryInput">Category</label>
                                <select class="form-control" id="employeeCategoryInput" name="employeeCategory">
                                    <option selected disabled>--Select Category--</option>
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="card-block">
                                <div class="card-title-block">
                                    <h3 class="title"> More Info </h3>
                                </div>
                                <ul class="nav nav-tabs nav-tabs-bordered">
                                    <li class="nav-item"> <a href="#officials" class="nav-link active" data-target="#officials" aria-controls="officials" data-toggle="tab" role="tab">Officials</a> </li>
                                    <li class="nav-item"> <a href="" class="nav-link" data-target="#service" aria-controls="service" data-toggle="tab" role="tab">Service</a> </li>
                                    <li class="nav-item"> <a href="" class="nav-link" data-target="#qualification" aria-controls="qualification" data-toggle="tab" role="tab">Qualifications</a> </li>
                                </ul>
                                <div class="tab-content tabs-bordered">
                                    <div class="tab-pane fade in active" id="officials">
                                        <h4>Officials</h4>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <label class="control-label" for="employeeOfficeInput">Office</label>
                                                    <select class="form-control" id="employeeOfficeInput" name="employeeOffice">
                                                        <option selected disabled>--Office--</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="control-label" for="employeeBranchInput">Branch</label>
                                                    <select class="form-control" id="employeeBranchInput" name="employeeBranch">
                                                        <option selected disabled>--Branch--</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="control-label" for="employeeSalaryAccountInput">Salary Account</label>
                                                    <input type="text" class="form-control" placeholder="Salary Account" id="employeeSalaryAccountInput" name="employeeSalaryAccount">
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="control-label" for="employeeSalaryHeadInput">Salary Head</label>
                                                    <select class="form-control" id="employeeSalaryHeadInput" name="employeeSalaryHead">
                                                        <option selected disabled>--Salary Head--</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="control-label" for="employeeWOGInput">Working On Ground</label>
                                                    <input type="text" class="form-control" placeholder="Woring On Ground" id="employeeWOGInput" name="employeeWOG">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="service">
                                        <h4>Service</h4>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <label class="control-label" for="employeeServiceNatureInput">Service Nature</label>
                                                    <select class="form-control" id="employeeServiceNatureInput" name="employeeServiceNature">
                                                        <option selected disabled>--Service Nature--</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label class="control-label" for="employeeDOAInput">Date Of Appointment</label>
                                                    <input type="date" class="form-control" placeholder="Date Of Appointment" id="employeeDOAInput" name="employeeDOA">
                                                </div>
                                                <div class="col-lg-4">
                                                    <label class="control-label" for="employeeSBLUDInput">Service Book Last Updated</label>
                                                    <input type="date" class="form-control" placeholder="Service Book Last Updated" id="employeeSBLUDInput" name="employeeSBLUD">
                                                </div>
                                                <div class="col-lg-4">
                                                    <label class="control-label" for="employeeMRBInput">Medical Record Book</label>
                                                    <select class="form-control" id="employeeMRBInput" name="employeeMRB">
                                                        <option selected disabled>--Medical Record--</option>
                                                        <option>Yes</option>
                                                        <option>No</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label class="control-label" for="employeeServiceCardInput">Service Card</label>
                                                    <select class="form-control" id="employeeServiceCardInput" name="employeeServiceCard">
                                                        <option selected disabled>--Service Card--</option>
                                                        <option>Yes</option>
                                                        <option>No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="qualification">
                                        <h4>Qualifications</h4>
                                        <div class="container">
                                            <div class="row">
                                                <label class="control-label" for="employeeQualificationInput">Qualification</label>
                                                <input type="text" class="form-control" placeholder="Qualifications" id="employeeQualificationInput" name="employeeQualification">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <fieldset class="form-group col-lg-12">
                            <button class="btn btn-success text-primary" >Add</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>
</article>
@endsection
