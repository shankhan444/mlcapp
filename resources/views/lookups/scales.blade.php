@extends('layouts.master')
@section('content')
    <article class="content forms-page">
        <div class="title-block">
            <h3 class="title"> Scale Lookup Form </h3>
        </div>
        <section class="section">
            <div class="row sameheight-container">
                <div class="col-md-12">
                    <div class="card card-block sameheight-item" style="height: 1600px">
                        <form role="form" method="post" action="">
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="scaleIdInput">Scale ID
                                    </label>
                                    <input type="text" class="form-control" id="scaleIdInput" name="scaleId" readonly>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="scaleInput">Scale </label>
                                    <input type="text" class="form-control" id="scaleInput" placeholder="Scale " name="scale" required>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="employeeClassInput">Employee Class</label>
                                    <input type="text" class="form-control" id="employeeClassInput" placeholder="Employee Class" name="employeeClass" required>
                                </div>
                            </div>
                            <fieldset class="form-group col-lg-12">
                                <button class="btn btn-success text-primary" name="addScale" type="submit" >Add Scale</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> All Scales </h3>
                            </div>
                            <section class="example">
                                <div class="table-flip-scroll">
                                    <table class="table table-striped table-bordered table-hover flip-content">
                                        <thead class="flip-header">
                                        <tr>
                                            <th>Scale ID</th>
                                            <th>Scale</th>
                                            <th>Employee Class</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Test Scale</td>
                                            <td>Test Employee Class</td>
                                            <td class="center">
                                                <a>
                                                    <i class="text-primary fa fa-edit"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@endsection
