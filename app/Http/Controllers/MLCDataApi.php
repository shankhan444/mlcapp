<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MLCDataApi extends Controller
{
    protected $connection;

    public function __construct(){
       $this->connection = DB::connection('oracle');
    }

    public function regions() {
        $regions = $this->connection->table('MLC.MLC_REGIONS')->get();
       foreach($regions as $key => $region) :
            $regions[$key]->offices = $this->connection->table('MLC.MLC_MAIN_OFFICE')->where('reg_id', $region->reg_id)->count();
        endforeach;
        return $regions;
    }

    public function province_regions($province_id) {
        $regions = $this->connection->table('MLC.MLC_REGIONS')->where('prov_id', $province_id)->get();
        return $regions;
    }

    public function provinces() {
        $provinces = $this->connection->table('MLC.MLC_PROVINCE')->get();
        foreach($provinces as $key => $province) :
            $provinces[$key]->regions = count($this->province_regions($province->prov_id));
        endforeach;
        return $provinces;
    }
}
