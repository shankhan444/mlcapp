@extends('layouts.master')
@section('content')
    <article class="content forms-page">
        <div class="title-block">
            <h3 class="title"> Accommodation Lookup Form </h3>
        </div>
        <section class="section">
            <div class="row sameheight-container">
                <div class="col-md-12">
                    <div class="card card-block sameheight-item" style="height: 1600px">
                        <form role="form" method="post" action="">

                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="accommodationIdInput">Accommodation ID
                                    </label>
                                    <input type="text" class="form-control" value="" id="accommodationIdInput" name="accommodationId" disabled>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="accommodationTypeSelect">Accommodation Type</label>
                                    <select class="form-control" id="accommodationTypeSelect" name="accommodationTypeId" required>
                                            <option selected disabled value="">--Select Type--</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="locationSelect">Location</label>
                                    <select class="form-control" id="locationSelect" name="locationId" required>
                                            <option selected disabled value="">--Select Location--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="propertyNumberInput">Property Number</label>
                                    <input type="text" class="form-control" id="propertyNumberInput" placeholder="Property Number" name="propertyNumber" required>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="yearBuiltInput">Year Built</label>
                                    <input type="date" class="form-control" id="yearBuiltInput" name="yearBuilt">
                                </div>
                                <fieldset class="form-group col-lg-4">
                                    <label class="control-label" for="addressInput">Address</label>
                                    <textarea type="text" class="form-control" id="addressInput" name="address" placeholder="Address"></textarea>
                                </fieldset>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="areaInput">Area</label>
                                    <input type="text" class="form-control" id="areaInput" placeholder="Area" name="area">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="areaStatus">Status</label>
                                    <select class="form-control" id="areaStatus" name="areaStatus" required>
                                        <option selected disabled value="">--Select Status--</option>
                                        <option value="Vacant">Vacant</option>
                                        <option value="Allocated">Allocated</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="dateOfAllotmentInput">Date Of Allotment</label>
                                    <input type="date" class="form-control" id="dateOfAllotmentInput" name="dateOfAllotment">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="allotteeNameInput">Name of Allottee</label>
                                    <select class="form-control" id="allotteeNameInput" name="employeeIdAsAllottee" required>
                                        <option selected disabled value="">--Select Allottee--</option>
                                        <option value="Vacant">Employee Name will Be Populated Here</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="nameOfOccupantSelect">Name Of Occupant</label>
                                    <input type="text" class="form-control" id="nameOfOccupantSelect" placeholder="Name Of Occupant" name="nameOfOccupant" >
                                </div>
                                <fieldset class="form-group col-lg-4">
                                    <label class="control-label" for="remarksInput">Remarks</label>
                                    <textarea type="text" class="form-control" id="remarksInput" name="remarks" placeholder="Remarks"></textarea>
                                </fieldset>
                            </div>
                            <fieldset class="form-group col-lg-12">
                                <button class="btn btn-success text-primary" name="addAccommodation" type="submit">Add</button>
                            </fieldset>
                            <input type="hidden" value="{{Session::token()}}" name="_token">
                        </form>
                    </div>
                </div>
            </div>
        </section>
        {{--<section class="section">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="card">--}}
                        {{--<div class="card-block">--}}
                            {{--<div class="card-title-block">--}}
                                {{--<h3 class="title"> All Accommodations </h3>--}}
                            {{--</div>--}}
                            {{--<section class="example">--}}
                                {{--<div class="table-flip-scroll">--}}
                                    {{--<table class="table table-striped table-bordered table-hover flip-content">--}}
                                        {{--<thead class="flip-header">--}}
                                        {{--<tr>--}}
                                            {{--<th>Accommodation ID</th>--}}
                                            {{--<th>Accommodation name</th>--}}
                                            {{--<th>Branch Name</th>--}}
                                            {{--<th>Branch Code</th>--}}
                                            {{--<th>Actions</th>--}}
                                        {{--</tr>--}}
                                        {{--</thead>--}}
                                        {{--<tbody>--}}
                                        {{--@foreach($accommodations as $accommodation)--}}
                                                {{--<tr class=" ">--}}
                                                    {{--<td>{{$accommodation->accommodation_id}}</td>--}}
                                                    {{--<td>{{$accommodation->accommodation_name}}</td>--}}
                                                    {{--<td>{{$accommodation->branch_name}}</td>--}}
                                                    {{--<td>{{$accommodation->branch_code}}</td>--}}
                                                    {{--<td class="center">--}}
                                                        {{--<a>--}}
                                                            {{--<i class="text-primary fa fa-edit"></i>--}}
                                                        {{--</a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                        {{--@endforeach--}}
                                        {{--</tbody>--}}
                                    {{--</table>--}}
                                {{--</div>--}}
                            {{--</section>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}
    </article>
@endsection
