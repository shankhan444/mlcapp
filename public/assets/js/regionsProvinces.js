$(function() {

    var $dashboardSalesBreakdownChart = $('#dashboard-sales-breakdown-chart');

    if (!$dashboardSalesBreakdownChart.length) {
        return false;
    }


   getProvinces();
    $(document).on("themechange", function(){
        getProvinces();
    });

});

function getProvinces()
{
    var url = 'http://192.168.100.200/mlcapp/api/provinces';
    $.ajax({
        url: url,
        type: "GET",
        beforeSend :function()
        {

        },
        success: function (result)
        {
            Morris.Donut({
                element: 'dashboard-sales-breakdown-chart',
                data: result,
                label:'prov_name',
                value:'regions',
                resize: true,
                colors: [
                    tinycolor(config.chart.colorPrimary.toString()).lighten(10).toString(),
                    tinycolor(config.chart.colorPrimary.toString()).darken(8).toString(),
                    config.chart.colorPrimary.toString()
                ],
            });
        }
    });
}