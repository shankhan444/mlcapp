@extends('layouts.master')
@section('content')
    <article class="content forms-page">
        <div class="title-block">
            <h3 class="title"> Degree Lookup Form </h3>
        </div>
        <section class="section">
            <div class="row sameheight-container">
                <div class="col-md-12">
                    <div class="card card-block sameheight-item" style="height: 1600px">
                        <form role="form" method="post" action="">
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="degreeLevelInput">Degree Name</label>
                                    <input type="text" class="form-control" id="degreeLevelInput" placeholder="Degree Name" name="degreeLevel" required>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="degreeStatusSelect">Is Active ?</label>
                                    <select type="text" class="form-control" id="degreeStatusSelect" name="degreeActive" required>
                                        <option selected disabled value="">--Select Status--</option>
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                                </div>
                            </div>
                            <fieldset class="form-group col-lg-12">
                                <button class="btn btn-success text-primary" name="addDegree " type="submit" >Add Degree </button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> All Degrees </h3>
                            </div>
                            <section class="example">
                                <div class="table-flip-scroll">
                                    <table class="table table-striped table-bordered table-hover flip-content">
                                        <thead class="flip-header">
                                        <tr>
                                            <th>Degree Name</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@endsection
