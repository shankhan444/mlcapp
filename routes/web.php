<?php

Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/employeeAdd', function () {
    return view('hr.employeeAdd');
})->name('employeeAdd');

//Bank Routes
Route::get('/bankLookup', 'MLCBanks@index')->name('bankLookup');
Route::post('/addBank', 'MLCBanks@store')->name('addBank');


//Designation Routes
Route::get('/DesignationsLookup', 'MLCDesignations@index')->name('DesignationsLookup');
Route::post('/addDesignation', 'MLCDesignations@store')->name('addDesignation');
Route::post('/updateDesignation', 'MLCDesignations@update')->name('updateDesignation');



//Data API
Route::get('/api/regions', 'MLCDataAPI@regions');
Route::get('/api/regions/{province_id}', 'MLCDataAPI@province_regions');
Route::get('/api/provinces', 'MLCDataAPI@provinces');

Route::get('/WOGLookup', function () {
    return view('lookups.WOG');
})->name('WOGLookup');
Route::get('/ScalesLookup', function () {
    return view('lookups.scales');
})->name('ScalesLookup');
Route::get('/GendersLookup', function () {
    return view('lookups.gender');
})->name('GendersLookup');
Route::get('/BranchesLookup', function () {
    return view('lookups.branches');
})->name('BranchesLookup');
Route::get('/DegreesLookup', function () {
    return view('lookups.degrees');
})->name('DegreesLookup');
Route::get('/InstitutesLookup', function () {
    return view('lookups.institutes');
})->name('InstitutesLookup');
Route::get('/SclaesLookup', function () {
    return view('lookups.institutes');
})->name('ScalesLookup');
Route::get('/ReligionsLookup', function () {
    return view('lookups.institutes');
})->name('ReligionsLookup');
Route::get('/InstitutesLookup', function () {
    return view('lookups.institutes');
})->name('RegionsLookup');
Route::get('/SpecializationLookup', function () {
    return view('lookups.institutes');
})->name('SpecializationLookup');
Route::get('/ProvincesLookup', function () {
    return view('lookups.institutes');
})->name('ProvincesLookup');
Route::get('/DistrictLookup', function () {
    return view('lookups.institutes');
})->name('DistrictLookup');
Route::get('/ServiceNatureLookup', function () {
    return view('lookups.institutes');
})->name('ServiceNatureLookup');
Route::get('/EmployeeCategoryLookup', function () {
    return view('lookups.institutes');
})->name('EmployeeCategoryLookup');
Route::get('/AccommodationLookup', function () {
    return view('lookups.accommodation');
})->name('AccommodationLookup');
Route::get('/AccommodationTypeLookup', function () {
    return view('lookups.accommodationTypes');
})->name('AccommodationTypeLookup');
Route::get('/BloodGroupLookup', function () {
    return view('lookups.bloodGroups');
})->name('BloodGroupLookup');
Route::get('/ClassificationOfCantonmentsLookup', function () {
    return view('lookups.institutes');
})->name('ClassificationOfCantonmentsLookup');
Route::get('/ChartsOfAccountLookup', function () {
    return view('lookups.institutes');
})->name('ChartsOfAccountLookup');
Route::get('/DistrictLookup', function () {
    return view('lookups.institutes');
})->name('DistrictLookup');
Route::get('/DisabilitiesLookup', function () {
    return view('lookups.disabilities');
})->name('DisabilitiesLookup');
Route::get('/EmployeeCategoryLookup', function () {
    return view('lookups.institutes');
})->name('EmployeeCategoryLookup');
Route::get('/InstitutesLookup', function () {
    return view('lookups.institutes');
})->name('InstitutesLookup');
Route::get('/LocationLookup', function () {
    return view('lookups.institutes');
})->name('LocationLookup');
Route::get('/MajorPenaltiesLookup', function () {
    return view('lookups.institutes');
})->name('MajorPenaltiesLookup');
Route::get('/MinorPenaltiesLookup', function () {
    return view('lookups.institutes');
})->name('MinorPenaltiesLookup');
Route::get('/MinorDisabilitiesLookup', function () {
    return view('lookups.institutes');
})->name('MinorDisabilitiesLookup');
Route::get('/OfficesLookup', function () {
    return view('lookups.institutes');
})->name('OfficesLookup');
Route::get('/PayDuringLeaveLookup', function () {
    return view('lookups.institutes');
})->name('PayDuringLeaveLookup');
Route::get('/PensionTypeLookup', function () {
    return view('lookups.institutes');
})->name('PensionTypeLookup');
Route::get('/RegionsLookup', function () {
    return view('lookups.institutes');
})->name('RegionsLookup');
Route::get('/ScaleLookup', function () {
    return view('lookups.institutes');
})->name('ScaleLookup');
Route::get('/ProvincesLookup', function () {
    return view('lookups.institutes');
})->name('ProvincesLookup');
Route::get('/ServiceNatureLookup', function () {
    return view('lookups.institutes');
})->name('ServiceNatureLookup');














Route::get('/oracle', function () {
    $newID = DB::connection('oracle')->select('select COALESCE(max(BANK_ID)+1, 1) as NEW from MLC.MLC_BANK ');
    dd($newID);
});
