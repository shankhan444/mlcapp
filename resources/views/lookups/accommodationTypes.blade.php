@extends('layouts.master')
@section('content')
    <article class="content forms-page">
        <div class="title-block">
            <h3 class="title"> Accommodation Type Lookup Form </h3>
        </div>
        <section class="section">
            <div class="row sameheight-container">
                <div class="col-md-12">
                    <div class="card card-block sameheight-item" style="height: 1600px">
                        <form role="form" method="post" action="">
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="accommodationTypeIdInput">Type ID
                                    </label>
                                    <input type="text" class="form-control" id="accommodationTypeIdInput" name="accommodationTypeId" readonly>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label class="control-label" for="accommodationLeaveSelect">Leave Name</label>
                                    <select class="form-control" id="accommodationLevelSelect" name="accommodationLeaveName" required>
                                        <option selected disabled value="">--Select Leave Name--</option>
                                        <option value="Flat">Flat</option>
                                        <option value="Bangalow">Bangalow</option>
                                        <option value="Quarter">Quarter</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <fieldset class="form-group col-lg-12 text-center">
                                <button class="btn btn-success " name="addAccommodationType " type="submit" >Add Accommodation Type </button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> All Accommodation Types </h3>
                            </div>
                            <section class="example">
                                <div class="table-flip-scroll">
                                    <table class="table table-striped table-bordered table-hover flip-content">
                                        <thead class="flip-header">
                                        <tr>
                                            <th>ID</th>
                                            <th>Type Name</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Test Accommodation Type </td>
                                            <td class="center">
                                                <a href="#">
                                                    <i class="text-primary fa fa-edit"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@endsection
